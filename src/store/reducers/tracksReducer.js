import {FETCH_TRACKS_SUCCESS, FETCH_HISTORY_TRACKS_SUCCESS} from '../actions/tracksActions';

const initialState = {
    tracks: [],
    historyTracks: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.tracks};

        case FETCH_HISTORY_TRACKS_SUCCESS:
            return {...state, historyTracks: action.data};

        default:
            return state;
    }
};

export default reducer;

