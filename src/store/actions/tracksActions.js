import axios from '../../axios';
import {push} from 'path';

export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const PLAY_SONG_SUCCESS = 'PLAY_SONG_SUCCESS';
export const FETCH_HISTORY_TRACKS_SUCCESS = 'FETCH_HISTORY_TRACKS_SUCCESS';

export const fetchTracksSuccess = tracks => {
    return {type: FETCH_TRACKS_SUCCESS, tracks};
};

export const fetchTracks = (id) => {
    return dispatch => {
        return axios.get(`/tracks?album=${id}`)
            .then(response => {
                dispatch(fetchTracksSuccess(response.data))
            })
    }
};

export const playSong = (song, token) => {
    return () => {
        return axios.post('/track_history', {track: song}, {headers: {"Token": token}})
    }
};


export const fetchHistoryTracksSuccess = (data) => {
    return {type: FETCH_HISTORY_TRACKS_SUCCESS, data};
};

export const fetchHistoryTracks = (token) => {
    return (dispatch) => {
        if(!token) {
            dispatch(push('/register'))
        } else {
            return axios.get('/track_history/history', {headers: {"Token": token}})
                .then(response => {
                    dispatch(fetchHistoryTracksSuccess(response.data))
                })
        }
    }
};