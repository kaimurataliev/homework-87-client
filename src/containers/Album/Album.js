import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchAlbums} from "../../store/actions/albumsActions";
import {Panel, Image, Button} from 'react-bootstrap'
import {NavLink} from 'react-router-dom';

class Album extends Component {

    componentDidMount() {
        const albumId = this.props.match.params.id;
        this.props.fetchAlbums(albumId);
    }

    render() {
        return (
            <Fragment>
                {this.props.albums.map((album, index) => {
                    return (
                        <Panel key={index}>
                            <Image
                                thumbnail
                                style={{width: '100px', marginRight: '50px'}}
                                src={"http://localhost:8000/uploads/" + album.image}
                            />
                            <strong style={{marginRight: '30px'}}>
                                Album: {album.title}
                            </strong>
                            <span style={{marginRight: '30px'}}>Artist: {album.performer.name}</span>
                            <span style={{marginRight: '30px'}}>Year: {album.year}</span>
                            <NavLink to={"/albums/" + album._id}>
                                <Button bsStyle="primary">
                                    Show tracks
                                </Button>
                            </NavLink>
                        </Panel>
                    )
                })}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAlbums: (id) => dispatch(fetchAlbums(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Album);