import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

import UserMenu from '../Menus/UserMenu';
import AnonMenu from '../Menus/AnonMenu';

const Toolbar = ({user, logout}) => (
    <Navbar bsStyle="inverse">
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Music</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <LinkContainer to="/" exact>
                    <NavItem>Artists</NavItem>
                </LinkContainer>

                <LinkContainer to="/history" exact>
                    <NavItem>Track history</NavItem>
                </LinkContainer>

                {user ? <UserMenu user={user} logout={logout} /> : <AnonMenu/>}

            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;